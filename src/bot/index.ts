import Twitch from 'twitch-js';

interface SurveyAnswer {
    answer: string,
    responses: string[]
}

interface Survey {
    disabled: boolean;
    active: boolean;
    completed: boolean;
    id: number;
    useIndex: boolean;
    question: string;
    answers: Array<SurveyAnswer>;
}

class twitchConfig {
    token: string;
    username: string;
    channel: string;
    clientId: string;
    lag: number;
    constructor() {
        //consume config passed in, or use defaults
        this.token = undefined;
        this.username = undefined;
        this.channel = undefined;
        this.clientId = process.env.TWITCH_CLIENTID;
        this.lag = 0;
    }
}

export default class Bot {
    config: twitchConfig;
    surveys: Array<Survey>;
    chat: any;
    api: any;
    chatConstants: any;
    currentSurvey: number;

    constructor() {
        this.config = new twitchConfig();
        this.surveys = [];
    }

    connect() {

        // Instantiate clients.
        const twitch = new Twitch(this.config);

        this.chat = twitch.chat;
        this.api = twitch.api;
        this.chatConstants = twitch.chatConstants;

        return this.chat.connect().then(() => {
            this.chat.join(this.config.channel);
            // this.chat.say(this.config.channel, BANNER.header);
            // this.chat.say(this.config.channel, BANNER.body);
        });
    }

    async exit() {
        return await {};
        // return this.chat.say(this.config.channel, 'GoodBye Now.');
    }

    setConfig(option: string, value: any) {

        return new Promise((resolve) => {
            this.config[option] = value;
            resolve(true)
        });
    }

    getConfig(option: string) {
        return this.config[option];
    }

    createSurvey(question, answers: Array<string>, useIndex) {

        return new Promise((resolve) => {
            const surveyAnswers: Array<SurveyAnswer> = [];
            answers.forEach((answer) => {
                surveyAnswers.push({
                    answer: answer,
                    responses: []
                })
            });
            const survey: Survey = {
                disabled: false,
                active: false,
                completed: false,
                id: this.surveys.length++ || 1,
                useIndex: useIndex || false,
                question: question,
                answers: surveyAnswers
            };
            this.surveys.push(survey);
            resolve(true);
        });
    }

    postSurvey(id) {
        return new Promise((resolve) => {
            const survey = this.surveys.filter((thisSurvey) => {
                return id == thisSurvey.id
            })[0];
            if (survey.active) {
                return;
            }
            this.currentSurvey = survey.id;
            survey.active = true;

            const chatLog = [];

            this.chat.say(this.config.channel, survey.question);
            setTimeout(() => {
                this.chat.say(this.config.channel, survey.answers.map(answer => (survey.answers.indexOf(answer)+1).toString() + '. ' + answer.answer).join(", "));
            }, 2001);

            this.chat.on(this.chatConstants.EVENTS.PRIVATE_MESSAGE, (data) => {
                if (survey.active) {
                    if (data.channel == `#${this.config.channel}`) {
                        chatLog.push({
                            channel: data.channel,
                            username: data.username,
                            message: data.message
                        });

                        if (survey.answers.filter(answer => {
                            return answer.responses.indexOf(data.tags.userId) >= 0
                        }).length === 0) {
                            try {
                                let answer = undefined;
                                if (survey.useIndex) {
                                    answer = survey.answers[parseInt(data.message) - 1];
                                } else {
                                    answer = survey.answers[survey.answers.findIndex((answer) => {
                                        return answer.answer.toLowerCase() === data.message.toLowerCase();
                                    })];
                                }

                                if (answer && answer.responses.indexOf(data.username) < 0) {
                                    answer.responses.push(data.tags.userId);
                                }
                            } catch {
                                // NOOP
                            }
                        }
                    }
                }
            });

            resolve(true);
        });
    }

    endSurvey(id) {
        return new Promise((resolve) => {
            const survey = this.surveys.filter((thisSurvey) => {
                return id == thisSurvey.id
            })[0];
            if (!survey.active) {
                return;
            }
            survey.active = false;
            setTimeout(() => {
                survey.completed = true;
                this.chat.say(this.config.channel, 'Polling has ended.');
                resolve(true);
            }, this.getConfig('lag') * 1000);
        });
    }

    resetSurvey(id) {
        return new Promise((resolve) => {
            const survey = this.surveys.filter((thisSurvey) => {
                return id == thisSurvey.id
            })[0];
            survey.active = false;
            survey.completed = false;
            survey.answers = survey.answers.map((answer) => {
                answer.responses = [];
                return answer;
            });
        });
    }

    listSurveys() {
        return this.surveys;
    }
}