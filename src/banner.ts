const Banner = {
    header: `TASurvey has joined chat.`,
    body: `Hello Chat. Please pay attention to the stream 
    for Surveys. We will collect you answers from chat.`
};

export default Banner;