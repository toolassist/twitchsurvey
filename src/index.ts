import Bot from './bot';
import { App } from './site';
import * as surveys from './surveys.json';

process.stdin.resume();//so the program will not close instantly

function exitHandler(options, exitCode) {
    if (exitCode || exitCode === 0) {
        if (options.exit) process.exit();
    }
}

//do something when app is closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {exit:true}));
process.on('SIGUSR2', exitHandler.bind(null, {exit:true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));

const bot = new Bot();

surveys.forEach((survey) => {
    if (!survey.disabled) {
        bot.createSurvey(survey.question, survey.answers, survey.useIndex);
    }
});

App.setConfig({
    port: process.env.PORT || 9090,
    bot: bot
});

App.start();