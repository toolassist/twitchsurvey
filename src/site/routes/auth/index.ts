import { Router, Request, Response, NextFunction } from 'express';
import * as passport from 'passport';
const router: Router = Router();

router.get('/',(req: Request, res: Response) => {
    res.redirect('/auth/twitch')
});

router.get('/twitch', passport.authenticate('twitch'));

router.get('/twitch/callback', (req: Request, res: Response, next: NextFunction) => {
    passport.authenticate('twitch', (err, user) => {
        if (err) {
            return res.redirect('/');
        }
        req.login(user, (err) => {
            if (err) {
                return res.redirect('/');
            }
            return res.redirect('/admin');
        })
    })(req, res, next);
});

export const AuthController: Router = router;