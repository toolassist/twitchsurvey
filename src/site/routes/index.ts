import { Router, Request, Response, NextFunction } from 'express';
import { AdminController } from "./admin";
import { AuthController } from "./auth";
import {App} from "../index";

const router: Router = Router();

router.use((req: Request, res: Response, next: NextFunction) => {
    res.locals.unAuthenticated = (!req.session.passport || !req.session.passport.user);
    next();
});
router.get('/', (req: Request, res: Response) => {

    const data = {
        surveys: App.bot.listSurveys().filter((survey) => {
            return survey.active || survey.completed;
        })
    };

    res.render('home', {
        page_title: "TASurvey",
        data: data
    });
});

router.get('/overlay', (req: Request, res: Response) => {
    res.render('overlay', {
        layout: false
    });
});

router.get('/currentSurvey', (req: Request, res: Response) => {
    const survey = App.bot.listSurveys().filter((survey) => {
        return survey.id === App.bot.currentSurvey;
    })[0] || {};

    res.json(survey);
});

router.use('/admin', AdminController);
router.use('/auth', AuthController);

export default router;