import { Router, Request, Response, NextFunction} from 'express';
import { App } from '../../';
const router: Router = Router();

router.use((req: Request, res: Response, next: NextFunction) => {
    if (!req.session.passport || !req.session.passport.user) {
        res.redirect('/auth');
        return;
    }
    next();
});

router.get('/',(req: Request, res: Response) => {
    const data = {
        surveys: App.getConfig().bot.listSurveys(),
        user: {
            name: req.session.passport.user.name
        },
        lag: App.bot.getConfig('lag')
    };

    res.render('admin', {
        page_title: "Admin Dashboard",
        data: data
    });
});

router.get( '/setup-bot', (req: Request, res: Response) => {
    res.render('setupBot', {
        page_title: "Admin Dashboard"
    });
});

router.post( '/setup-bot', (req: Request, res: Response) => {
    App.bot.setConfig('username', req.body.botName).then(() => {
        App.bot.setConfig('channel', req.body.channel).then( () => {
            App.bot.setConfig('token', req.body.token).then(() => {
                App.bot.connect();
                res.redirect('/admin');
            });
        });
    });
});

router.post('/survey', (req: Request, res: Response) => {
    App.bot.createSurvey(req.body.question,req.body.answers.split(','),req.body.useIndex).then(() => {
        res.redirect('/admin');
    });
});

router.post('/start-survey', (req: Request, res: Response) => {
    App.bot.postSurvey(req.body.id).then(() => {
        res.redirect('/admin');
    });
});

router.post('/end-survey', (req: Request, res: Response) => {
    App.bot.endSurvey(req.body.id).then(() => {
        res.redirect('/admin');
    });
});

router.post('/lag-offset', (req: Request, res: Response) => {
    App.bot.setConfig('lag', req.body.lagOffset).then(() => {
        res.redirect('/admin');
    });
});

router.post('/reset-survey', (req: Request, res: Response) => {
   App.bot.resetSurvey(req.body.id).then(() => {
       res.redirect('/admin');
   });
});

export const AdminController: Router = router;