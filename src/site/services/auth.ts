import * as passport from 'passport';
import authorized_users from '../authorized_users';

interface User {
    name: string;
}

export default class loaded{
    constructor() {
        console.log('Auth Service Loaded');
    }
};

const fetchUser = (() => {
    // This is an example! Use password hashing in your project and avoid storing passwords in your code
    const users: User[] = authorized_users;
    return async function(username) {
        return users.filter((user) => {
            return user.name === username;
        })[0];
    }
})();

passport.serializeUser(function(user: User, done) {
    done(null, user);
});

passport.deserializeUser(async function(sessionUser, done) {
    try {
        const user = await fetchUser(sessionUser.name);
        done(null, user);
    } catch(err) {
        done(err);
    }
});

const TwitchStrategy = require('passport-twitch').Strategy;
passport.use(new TwitchStrategy({
        clientID: process.env.TWITCH_CLIENTID,
        clientSecret: process.env.TWITCH_CLIENTSECRET,
        callbackURL: 'https://tas.bot/auth/twitch/callback',
        scope: "user_read"
    },
    function(token, tokenSecret, profile, done) {

        // retrieve user ...
        fetchUser(profile.username).then((user) => {
            done(null, user);
        });
    }
));