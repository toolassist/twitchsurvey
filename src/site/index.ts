import * as express from 'express';
import * as handlebars from 'express-handlebars';
import * as session from 'express-session';
import * as logger from 'morgan';
import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import * as passport from 'passport';
import * as uuid from 'uuid';
import router from './routes';
import AuthService from './services/auth';

new AuthService();

const app = express();

const hbs = handlebars.create({
    extname      : '.hbs',
    layoutsDir   : __dirname + '/views/layouts',
    defaultLayout: 'single',
    partialsDir  : [__dirname + '/views/partials'],
    helpers      : {
        inc: (value) => parseInt(value) + 1
    }
});
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views/pages');
app.disable('view cache');

app.use(logger('dev'));
app.use('/static', express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    genid: function(req) {
        return uuid(); // use UUIDs for session IDs
    },
    cookie: {
        httpOnly: true,
        maxAge: 86400000
    }
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', router);

export namespace App {
    export let config = {
        port: undefined,
        bot: undefined
    };

    export let bot = undefined;

    export function start() {
        app.listen(App.config.port, () => console.log(`Twitch Survey Admin listening on port ${App.config.port}!`));
    }

    export function setConfig(config?) {
        App.config = config;
        App.bot = config.bot;
    }

    export function getConfig() {
        return App.config;
    }

}